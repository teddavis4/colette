#!/usr/bin/env python

import logging
import json
import datetime
import traceback


class BotLogger(logging.getLoggerClass()):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setLevel(logging.INFO)

    def do_log(self, log_level, message, *args, **kwargs):
        log = {
            "log_level": log_level,
            "date": datetime.datetime.now().strftime("%Y%m%dT%H:%M:%S")
        }
        if isinstance(message, dict):
            log.update(message)
        elif isinstance(message, BaseException):
            log['message'] = message.msg
        else:
            log['message'] = message
        self.log(logging.INFO, json.dumps(log), *args, **kwargs)

    def info(self, message, *args, **kwargs):
        self.do_log(logging.INFO, message, *args, **kwargs)

    def debug(self, message, *args, **kwargs):
        self.do_log(logging.INFO, message, *args, **kwargs)

    def warning(self, message, *args, **kwargs):
        self.do_log(logging.INFO, message, *args, **kwargs)

    def error(self, message, *args, **kwargs):
        self.do_log(logging.INFO, message, *args, **kwargs)

    def exception(self, message, *args, **kwargs):
        self.do_log(logging.INFO, message, *args, **kwargs)
