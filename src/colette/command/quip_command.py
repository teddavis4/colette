#!/usr/bin/env python

from .command import BotCommand
from ..db import BotDB
from googleapiclient.discovery import build


class QuipCommand(BotCommand):
    def __init__(self, name='google'):
        super().__init__(name=name)
        self.config = {}
        self.extras = {}

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        if not value:
            self._text = None
        if isinstance(value, str):
            self._text = value
        else:
            ValueError('quip must be a string')

    def execute(self):
        subcommand = 'save'
        if self.text:
            subcommand = self.text.split(' ', 1)[0]
            self.text = self.text.split(' ', 1)[1]
        self.handle_quip_command(subcommand)

    def handle_quip_command(self, subcommand):
        subcommands = {
            'save': self.save_quip,
        }
        return subcommands.get(subcommand, self._null)()

    def save_quip(self):
        update = self.extras.get('update')
        db = BotDB(self.config.get('database_file'))

        quip = update.message.reply_to_message.text
        scribe = update.message.reply_to_message.from_user.id
        quip_date = update.message.reply_to_message.date
        owner = update.message.reply_to_message.from_user.username
        chat_id = update.message.chat.id
        chat_name = update.message.chat.title
        #self.user.check_user_exist(owner, username)
        self.log.info({'message': 'saving new quip',
                       'quip': quip,
                       'quip_date': quip_date.strftime("%Y%m%dT%H:%M:%S"),
                       'scribe': scribe,
                       'owner': owner,
                       'chat_name': chat_name,
                       'chat_id': chat_id})
        #c.execute('insert into {0} (date, owner, quote, room) values (?, ?, ?, ?)'.format(db), (quip_date, owner, quip, chat_id))
        db.write_to_table(self.config.get('quip_table'), {'date': quip_date.strftime("%Y%m%dT%H:%M:%S"),
                                                          'scribe': scribe,
                                                          'owner': owner,
                                                          'chat_name': chat_name,
                                                          'chat_id': chat_id,
                                                          'quip': quip
                                                          })
        return True

    def _null(self):
        return False
