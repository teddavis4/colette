#!/usr/bin/env python

from .command import BotCommand
from googleapiclient.discovery import build

class GoogleCommand(BotCommand):
    def __init__(self, name='google'):
        super().__init__(name=name)
        self.config = {}

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        if not value:
            self._text = None
        if isinstance(value, str) and len(value) < 100:
            self._text = value
        else:
            ValueError('query must be a string between 1 and 100 characters')

    def execute(self):
        self.log.info({'message': 'searching google',
                       'query': self.text})
        search_results = google_search(self.text, self.config.get('google_api_key'), self.config.get('google_cse_id'))
        result = search_results[0]
        self.log.debug({'message': 'google search results',
                        'result': result})
        return result.get('link')


def google_search(search_term, api_key, cse_id, **kwargs):
    service = build("customsearch", "v1", developerKey=api_key, cache_discovery=False)
    res = service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()
    return res['items']
