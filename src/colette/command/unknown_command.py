#!/usr/bin/env python

from .command import BotCommand


class UnknownCommand(BotCommand):
    def __init__(self, name='unknown'):
        super().__init__(name=name)

    def execute(self):
        self.log.info({'message': 'encountered unknown command',
                       'command': self.name,
                       'text': self.text})
        return 'unknown command'
