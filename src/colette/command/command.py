#!/usr/bin/env python

import logging
from ..exceptions import ValidationError

class BotCommand:
    def __init__(self, name='helloworld'):
        self.name = name
        self.text = None
        self.log = logging.getLogger('Bot.BotCommand').getChild(name)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if isinstance(value, str) and len(value) < 16:
            self._name = value
        else:
            raise ValueError('must provide a string between 1 and 16 characters')

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, value):
        if value:
            raise ValueError('command does not take a value')
        self._text = value

    def execute(self):
        raise NotImplementedError
