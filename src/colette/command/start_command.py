#!/usr/bin/env python

from .command import BotCommand

class StartCommand(BotCommand):
    def __init__(self, name='start'):
        super().__init__(name=name)

    def execute(self):
        self.log.info('ran start command')
        return "hello"

