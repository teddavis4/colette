#!/usr/bin/env python

import sqlite3
import logging

class BotDB:
    def __init__(self, database_file=None):
        if not database_file:
            database_file = ':memory:'
        self.db_conn = sqlite3.connect(database_file)
        self.log = logging.getLogger('Bot.BotDB')

    def write_to_table(self, table, values):
        sql_keys = ''
        sql_values = []
        for k, v in values.items():
            if sql_keys:
                sql_keys += f', {k}'
            else:
                sql_keys = k
            sql_values.append(v)
        keyph = ('?,'*len(sql_keys))[:-1]
        valuesph = ('?,'*len(sql_values))[:-1]
        cur = self.db_conn.cursor()
        self.log.debug({'message': 'executing write_to_table',
                        'sql': f'INSERT INTO {table}({keyph}) values ({valuesph})',
                        'keys': sql_keys,
                        'values': sql_values})
        cur.execute(f'INSERT INTO {table}({sql_keys}) values ({valuesph})', sql_values)
        self.db_conn.commit()
        cur.close()

