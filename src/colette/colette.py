#!/usr/bin/env python

import logging
import traceback

import yaml

from telegram import InlineQueryResultArticle, InputTextMessageContent, ParseMode
from telegram.ext import CommandHandler, Filters, InlineQueryHandler, MessageHandler, Updater

from .command import StartCommand, UnknownCommand, GoogleCommand, QuipCommand
from .log import BotLogger
from .quote.picture_quote import PictureQuoter
from .quote.text_quote import TextQuoter

logging.setLoggerClass(BotLogger)


class Bot:
    def __init__(self, name='colette', config_path=None):
        self.name = name
        self.config = self.load_config(config_path=config_path) or {}
        self.logger = logging.getLogger('Bot')
        self.logger.addHandler(logging.StreamHandler())
        self.logger.addHandler(logging.FileHandler('colette.log'))
        if self.config.get('token'):
            self.updater = Updater(self.config.get('token'))
            self.updater.dispatcher.add_handler(MessageHandler(Filters.command, self.handle_bot_command))

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        if isinstance(value, str) and len(value) < 24:
            self._name = value
        else:
            raise ValueError('name must be a string between 1 and 24 characters')

    def load_config(self, config_path=None):
        config = {}
        if not config_path:
            config_path = f"{self.name}.conf"
        with open(config_path) as raw_config:
            config = yaml.load(raw_config)
        return config

    def handle_bot_command(self, bot, update):
        text = None
        raw_text = update.message.text
        split_text = raw_text.split(' ', 1)
        command = raw_text.split(' ', 1)[0][1:]
        if len(split_text) >=2:
            text = split_text[1]
        extras = {'bot': bot,
                  'update': update}
        command_output = self.handle_command(command, text, extras)
        if isinstance(command_output, str):
            bot.sendMessage(update.message.chat_id, text=command_output)

    def handle_command(self, command, text=None, extras={}):
        self.logger.info({'message': 'processing command',
                          'command': command})
        commands = {
            'start': StartCommand(),
            'google': GoogleCommand(),
            'quip': QuipCommand(),
        }
        cmd = commands.get(command, UnknownCommand())
        try:
            cmd.text = text
            cmd.config = self.config
            cmd.extras = extras
            return cmd.execute()
        except Exception as exc:
            self.logger.error({'message': 'error processing command',
                               'error': traceback.format_exc(limit=20)})
            return "I couldn't process that command. If this seems like a bug then contact the maintainer"

    def run(self):
        self.updater.start_polling()
        self.updater.idle()
