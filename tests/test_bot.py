#!/usr/bin/env python

import os

import pytest
from colette import Bot


@pytest.fixture
def bot():
    bot = Bot(name='testbot', config_path=os.path.join(os.path.dirname(__file__), 'test_data', 'testbot.conf'))
    return bot


def test_handle_unknown_command(bot):
    command = 'unknowncommand'
    out = bot.handle_command(command)
    assert out == 'unknown command'
