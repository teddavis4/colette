#!/usr/bin/env python

import colette
import logging
import io
import json

def test_get_logger():
    new_logger = logging.getLogger('test_logger')
    assert isinstance(new_logger, colette.log.BotLogger)

def test_write_info_log():
    logfile = io.StringIO()
    new_logger = logging.getLogger('test_logger')
    shandler = logging.StreamHandler(logfile)
    new_logger.addHandler(shandler)
    new_logger.info('just a test')
    logfile.seek(0)
    log_out = json.load(logfile)
    assert log_out['log_level'] == logging.INFO
    assert log_out['message'] == 'just a test'
    
