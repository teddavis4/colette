#!/usr/bin/env python

import os
from unittest.mock import MagicMock, patch

import pytest
import colette
from colette import Bot


@pytest.fixture
def bot():
    bot = Bot(name='testbot', config_path=os.path.join(os.path.dirname(__file__), 'test_data', 'testbot.conf'))
    return bot

@patch.object(colette.command.google_command.GoogleCommand, 'execute')
def test_handle_google_command(mock_google_search, bot):
    command = 'google'
    text = 'cnn'
    out = bot.handle_command(command, text)
    mock_google_search.assert_called_once()
