#!/usr/bin/env python

import pytest
from colette.db import BotDB


@pytest.fixture
def db():
    bot_db = BotDB()
    return bot_db


def test_write_to_table(db):
    cur = db.db_conn.cursor()
    cur.execute('CREATE TABLE testtable(id integer primary key, c text);')
    db.db_conn.commit()
    db.write_to_table('testtable', {'c': 'this is a test'})
    cur.execute('SELECT * from testtable;')
    res = cur.fetchone()
    assert res[0] == 1
    assert res[1] == 'this is a test'
